const functions = require("firebase-functions");
const express = require("express");
const firebaseAdmin = require("firebase-admin");
const globeLabsApi = require("globe-connect");
const expressApp = express();
const serviceAccount = require(
    "./croppy-net-11be2-firebase-adminsdk-yq58g-2262a3a37b.json"
);
firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: "https://croppy-net-11be2.firebaseio.com"
});
const database = firebaseAdmin.firestore();
const subscribe = require('./expressget');

expressApp.get("/subscribe", (request, response) => {
    const subscriberData = {
        userName: `+63${request.query.subscriber_number}`,
        accessToken: request.query.access_token,
        accountType: "Irregular"
    };
    const addData = database.collection("Users").add(subscriberData);
});

expressApp.post("/notify", (request, response) => {
    const jsonParsed = request.body;
    const inboundSMSMessage = jsonParsed.inboundSMSMessageList
        .inboundSMSMessage[0];
    const message = inboundSMSMessage.message;
    const senderAddress = inboundSMSMessage.senderAddress;
    let trueAddress = senderAddress.substr(4);
    console.log ("Address", trueAddress);
    let sendAccessToken;
    let holderOfNoUse = database.collection("Users")
        .where ("userName", "==", trueAddress)
        .get()
        .then( querySnapshot => {
            if (querySnapshot.empty) {
                console.log("snap ", querySnapshot);
                console.log("token ", sendAccessToken);
                throw new Error("You are not registered.");
            } 
            else {
                querySnapshot.forEach(doc => {
                    sendAccessToken = doc.data().accessToken;
                    console.log("HEY HEY HEY")
                console.log(sendAccessToken);
                const sms = globeLabsApi.Sms(
                    "21586053", sendAccessToken
                );
                sms.setReceiverAddress(trueAddress);
                let messageResponse = "";

                if (message.toLowerCase() === "prod") {
                    const getDate = database.collection("Products").get()
                        .then(snapshot => {
                            if (snapshot.empty) {
                                throw new Error("There are no products available.");
                            } else {
                                snapshot.forEach(doc => {
                                    messageResponse = messageResponse + `${doc.data().title}\n`;
                                    console.log(messageResponse);
                                });
                                sms.setMessage(messageResponse);
                                sms.sendMessage();
                                return;
                            }
                        })
                        .catch(err => {
                            sms.setMessage(err);
                        });
                } else {
                    messageResponse = "Invalid command. Please send a valid command.";
                }
                });
                
            }
            return;
        });

});

exports.globeLabs = functions.https.onRequest(expressApp);