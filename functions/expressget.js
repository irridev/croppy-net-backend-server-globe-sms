const functions = require("firebase-functions");
const express = require("express");
const firebaseAdmin = require("firebase-admin");
const globeLabsApi = require("globe-connect");
const expressApp = express();

subscribe = expressApp.get("/subscribe", (request, response) => {
    const subscriberData = {
        userName: `+63${request.query.subscriber_number}`,
        accessToken: request.query.access_token,
        accountType: "Irregular"
    };
    const addData = database.collection("Users").add(subscriberData);
});

module.exports.subscribe = subscribe;