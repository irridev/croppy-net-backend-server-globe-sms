const functions = require("firebase-functions");
const express = require("express");
const firebaseAdmin = require("firebase-admin");

const serviceAccount = require(
    "./croppy-net-11be2-firebase-adminsdk-yq58g-2262a3a37b.json"
);

firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: "https://croppy-net-11be2.firebaseio.com"
});

const database = firebaseAdmin.firestore();
